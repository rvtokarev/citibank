package com.citibank.demo;

import org.springframework.jms.annotation.JmsListener;

import java.util.HashMap;
import java.util.Map;

public class PriceProcessorImpl implements PriceProcessor {

    private final String ccyPair;

    private static final Map<String, PriceProcessor> priceProcessorMap =
            new HashMap();

    public PriceProcessorImpl(String ccyPair) {
        this.ccyPair = ccyPair;
    }



    @JmsListener(destination = "priceQueueu")
    private void receivePrice(Message message){
        priceProcessorMap.get(message.getCcyPair()).onPrice(message.getCcyPair(), message.getRate());
    }


    @Override
    public void onPrice(String ccyPair, double rate) {
        //todo
    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        priceProcessorMap.put(priceProcessor.getCcyPair(), priceProcessor);
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        priceProcessorMap.remove(priceProcessor.getCcyPair());

    }

    @Override
    public String getCcyPair() {
        return ccyPair;
    }
}
