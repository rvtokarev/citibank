package com.citibank.demo;

import lombok.Data;

@Data
public class Message {
    private String ccyPair;
    private Double rate;
}
